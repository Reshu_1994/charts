import * as React from "react";
import "./App.css";
import HorizontalBarChart from "./HorizontalBarChart";
import PieChart from "./pieChart";
import VerticalBarChart from "./VerticalBarChart";
import DoughnutChart from "./DoughnutChart";
import LargerDoughnut from "./LargerDoughnut";
import { DashboardData } from "./jsonData";
import moment from "moment";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import ReactWordcloud from "react-wordcloud";
import NetworkGraph from "./Network";

function App() {
  const [responseArray, setResponseArray] = React.useState({});
  const [highRequestRate, setHighRequestRate] = React.useState({});
  const [tempHighRequestRate, settempHHighRequestRate] = React.useState({});
  const [averageRequests, setAverageRequests] = React.useState({
    January: 0,
    February: 0,
    March: 0,
    April: 0,
    May: 0,
    June: 0,
    July: 0,
    August: 0,
    September: 0,
    October: 0,
    November: 0,
    December: 0,
  });
  const [noResponse, setNoResponse] = React.useState({
    January: 0,
    February: 0,
    March: 0,
    April: 0,
    May: 0,
    June: 0,
    July: 0,
    August: 0,
    September: 0,
    October: 0,
    November: 0,
    December: 0,
  });
  const [ResponseMoreThan3Days, setResponseMoreThan3Days] = React.useState({
    January: 0,
    February: 0,
    March: 0,
    April: 0,
    May: 0,
    June: 0,
    July: 0,
    August: 0,
    September: 0,
    October: 0,
    November: 0,
    December: 0,
  });
  const [requestPerProd, setRequestPerProd] = React.useState({});
  const [count, setCount] = React.useState(0);
  const [cloud, setCloud] = React.useState([]);

  const [panelState, setPanelState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const [checked, setChecked] = React.useState([]);

  const handleChange = (event, data) => {
    if (event.target.checked) {
      setChecked((prev) => [...prev, data]);
    } else {
      setChecked((prev) => prev.filter((item) => item !== data));
    }
  };

  React.useEffect(() => {
    initialLoad();
  }, []);

  const initialLoad = () => {
    let respArray = {}; //No of Verbal/written response with respect to country
    let highReqRate = {};
    let avgRequests = {};
    let notResponded = {};
    let responseMorethan3 = {};
    let reqPerProduct = {};
    let arrOfWords = [];

    DashboardData.map((data, key, array) => {

      //No of Verbal/written response with respect to country
      if (respArray.hasOwnProperty(data.Country)) {
        respArray = {
          ...respArray,
          [data["Country"]]: {
            verbal:
              parseInt(respArray[data.Country].verbal) +
              parseInt(data["Verbal Response"]),
            written:
              parseInt(respArray[data.Country].written) +
              parseInt(data["Written Response"]),
          },
        };
      } else {
        respArray = {
          ...respArray,
          [data["Country"]]: {
            verbal: parseInt(data["Verbal Response"]),
            written: parseInt(data["Written Response"]),
          },
        };
      }

      // Highest request rate based on country
      if (highReqRate.hasOwnProperty(data.Country)) {
        highReqRate = {
          ...highReqRate,
          [data["Country"]]: parseInt(highReqRate[data.Country]) + 1,
        };
      } else {
        highReqRate = { ...highReqRate, [data["Country"]]: 1 };
      }

      //Average no of request per month
      let openDate = moment(data["Date/Time Opened"]).format("YYYY/MM/DD");
      let monthCheck = moment(openDate, "YYYY/MM/DD").format("M");
      var monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      if (avgRequests.hasOwnProperty(monthNames[monthCheck - 1])) {
        avgRequests = {
          ...avgRequests,
          [monthNames[monthCheck - 1]]:
            parseInt(avgRequests[monthNames[monthCheck - 1]]) + 1,
        };
      } else {
        avgRequests = { ...avgRequests, [monthNames[monthCheck - 1]]: 1 };
      }

      //No of request not responded per month
      // if(noResponse.hasOwnProperty(monthNames[monthCheck - 1])){
      //  setNoResponse({
      //           ...noResponse,
      //           [monthNames[monthCheck - 1]]:
      //             parseInt(noResponse[monthNames[monthCheck - 1]]) + 1,
      //         });
      // }
      // else{
      //   setNoResponse({
      //     ...noResponse,
      //     [monthNames[monthCheck - 1]]:
      //       0,
      //   });
      // }
      if (parseInt(data["No Response"]) === 1) {
        //notResponded
        if (notResponded.hasOwnProperty(monthNames[monthCheck - 1])) {
          notResponded = {
            ...notResponded,
            [monthNames[monthCheck - 1]]:
              parseInt(notResponded[monthNames[monthCheck - 1]]) + 1,
          };
        } else {
          notResponded = { ...notResponded, [monthNames[monthCheck - 1]]: 1 };
        }
      }

      //No of Response time taken more than 3 days per month.
      let startDate = moment(data["Date/Time Opened"]); //.format("DD.MM.YYYY");
      let endDates = moment(data["Date/Time Closed"]); //.format("DD.MM.YYYY");
      let result = endDates.diff(startDate, "days");

      if (responseMorethan3.hasOwnProperty(monthNames[monthCheck - 1])) {
        responseMorethan3 = {
          ...responseMorethan3,
          [monthNames[monthCheck - 1]]:
            parseInt(result) > 3
              ? parseInt(responseMorethan3[monthNames[monthCheck - 1]]) + 1
              : parseInt(responseMorethan3[monthNames[monthCheck - 1]]),
        };
      } else {
        responseMorethan3 = {
          ...responseMorethan3,
          [monthNames[monthCheck - 1]]: parseInt(result) > 3 ? 1 : 0,
        };
      }

      //No of Request per product
      if (reqPerProduct.hasOwnProperty(data.Product)) {
        reqPerProduct = {
          ...reqPerProduct,
          [data["Product"]]: parseInt(reqPerProduct[data.Product]) + 1,
        };
      } else {
        reqPerProduct = { ...reqPerProduct, [data["Product"]]: 1 };
      }

      //Word Cloud
      arrOfWords.push(data["Therapeutic Area"]);
      arrOfWords.push(data["Country"]);
      arrOfWords.push(data["Case Origin"]);
      arrOfWords.push(data["Type"]);
    });

    let wordCloud = [...new Set(arrOfWords)];
    let words = [];
    wordCloud.map((input) => {
      words.push({ text: input, value: "20" });
    });

    Object.entries(notResponded).map((input) => {
      if (noResponse.hasOwnProperty(input[0])) {
        setNoResponse(prev => ({ ...prev, [input[0]]: input[1] }));
      }
    });
    Object.entries(avgRequests).map((input1) => {
      if (averageRequests.hasOwnProperty(input1[0])) {
        setAverageRequests(prev => ({ ...prev, [input1[0]]: input1[1] }));
      }
    });
    Object.entries(responseMorethan3).map((input2) => {
      if (ResponseMoreThan3Days.hasOwnProperty(input2[0])) {
        setResponseMoreThan3Days(prev => ({ ...prev, [input2[0]]: input2[1] }));
      }
    });

    setResponseArray(respArray);
    setHighRequestRate(highReqRate);
    // setAverageRequests(avgRequests);
    // setNoResponse({...noResponse, notResponded});
    setRequestPerProd(reqPerProduct);
    setCloud(words);
  };

  const list = () => (
    <div
      className="d-flex"
      style={{
        padding: "16px",
        height: "100%",
        width: "320px",
        flexDirection: "column",
      }}
    >
      <h6>Request Rate</h6>
      <div className="d-flex">
        <FormGroup>
          <FormControlLabel
            control={<Checkbox />}
            label="UK"
            onChange={(e) => handleChange(e, "United Kingdom")}
          />
          <FormControlLabel
            control={<Checkbox />}
            label="US"
            onChange={(e) => handleChange(e, "United States")}
          />
          <FormControlLabel
            control={<Checkbox />}
            label="Brazil"
            onChange={(e) => handleChange(e, "Brazil")}
          />
        </FormGroup>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox />}
            label="Canada"
            onChange={(e) => handleChange(e, "Canada")}
          />
          <FormControlLabel
            control={<Checkbox />}
            label="India"
            onChange={(e) => handleChange(e, "India")}
          />
          <FormControlLabel
            control={<Checkbox />}
            label="Austrailia"
            onChange={(e) => handleChange(e, "Australia")}
          />
        </FormGroup>
      </div>
      <Button
        variant="contained"
        onClick={onClickApplyFilter}
        style={{ marginTop: "auto", marginBottom: "16px" }}
      >
        Apply
      </Button>
    </div>
  );

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setPanelState({ ...panelState, [anchor]: open });
  };

  const onClickApplyFilter = () => {
    let tempObj = {};
    checked.map((item) => {
      tempObj[item] = highRequestRate[item];
    });
    settempHHighRequestRate(tempObj);
  };

  return (
    <div className="container">
      <div className="d-flex align-items-center justify-space-between">
        <h2>DASHBOARD</h2>
        <Button variant="contained" onClick={toggleDrawer("left", true)}>
          Filter
        </Button>
      </div>
      <Drawer
        anchor={"left"}
        open={panelState["left"]}
        onClose={toggleDrawer("left", false)}
      >
        {list()}
      </Drawer>

      <div className="d-flex">
        <div className="col-9">
          <div className="d-flex">
            <div className="col-7">
              <HorizontalBarChart responseArray={responseArray} />
            </div>
            <div className="col-5">
              <PieChart
                highRequestRate={
                  Object.keys(tempHighRequestRate).length !== 0
                    ? tempHighRequestRate
                    : highRequestRate
                }
              />
            </div>
          </div>

          <div
            className="shadow-box col-12"
            style={{ width: "96%", marginTop: "30px" }}
          >
            <VerticalBarChart noResponse={noResponse} averageRequests = {averageRequests} ResponseMoreThan3Days={ResponseMoreThan3Days}/>
          </div>

          <div className="shadow-box col-12">
            {/* <NetworkGraphChart /> */}
            <NetworkGraph />
          </div>
        </div>
        <div className="col-3">
          <h6>Request per Product</h6>

          {Object.entries(requestPerProd).length !== 0 && (
            <div>
              <div className="d-flex">
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[0][0]}
                  color={"#ffcbf2"}
                  data={Object.entries(requestPerProd)[0][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[1][0]}
                  color={"#f3c4fb"}
                  data={Object.entries(requestPerProd)[1][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[2][0]}
                  color={"#ecbcfd"}
                  data={Object.entries(requestPerProd)[2][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[3][0]}
                  color={"#e5b3fe"}
                  data={Object.entries(requestPerProd)[3][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[4][0]}
                  color={"#e2afff"}
                  data={Object.entries(requestPerProd)[4][1]}
                />
              </div>
              <div className="d-flex" style={{ marginTop: "10px" }}>
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[5][0]}
                  color={"#e0c3fc"}
                  data={Object.entries(requestPerProd)[5][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[6][0]}
                  color={"#ddbdfc"}
                  data={Object.entries(requestPerProd)[6][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[7][0]}
                  color={"#dab6fc"}
                  data={Object.entries(requestPerProd)[7][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[8][0]}
                  color={"#cbb2fe"}
                  data={Object.entries(requestPerProd)[8][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[9][0]}
                  color={"#bbadff"}
                  data={Object.entries(requestPerProd)[9][1]}
                />
              </div>

              <div className="d-flex" style={{ marginTop: "10px" }}>
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[10][0]}
                  color={"#ada7ff"}
                  data={Object.entries(requestPerProd)[10][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[11][0]}
                  color={"#9fa0ff"}
                  data={Object.entries(requestPerProd)[11][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[12][0]}
                  color={"#8e94f2"}
                  data={Object.entries(requestPerProd)[12][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[13][0]}
                  color={"#8187dc"}
                  data={Object.entries(requestPerProd)[13][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[14][0]}
                  color={"#757bc8"}
                  data={Object.entries(requestPerProd)[14][1]}
                />
              </div>

              <div className="d-flex" style={{ marginTop: "10px" }}>
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[15][0]}
                  color={"#b185db"}
                  data={Object.entries(requestPerProd)[15][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[16][0]}
                  color={"#9163cb"}
                  data={Object.entries(requestPerProd)[16][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[17][0]}
                  color={"#815ac0"}
                  data={Object.entries(requestPerProd)[17][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[18][0]}
                  color={"#7251b5"}
                  data={Object.entries(requestPerProd)[18][1]}
                />
                <DoughnutChart
                  prodLabel={Object.entries(requestPerProd)[19][0]}
                  color={"#6247aa"}
                  data={Object.entries(requestPerProd)[19][1]}
                />
              </div>
            </div>
          )}
          <div>
            <h6>Request per Product type</h6>
            <LargerDoughnut
              prodLabel={"Detail"}
              color={"#e2afff"}
              data={"100"}
            />
          </div>
          <div>
            <ReactWordcloud words={cloud} size={[200, 200]} />
          </div>
        </div>
      </div>
    </div>

    // <div className="App">
    // </div>
  );
}

export default App;
