import * as React from 'react';
import { Bar } from "react-chartjs-2";

const VerticalBarChart = (props) => {
    console.log("Propsss", Object.values(props.averageRequests), Object.values(props.noResponse));
    return (
        <div>
            <h6 >Per Month Response Analysis</h6>
            <Bar 
                data={{
                    labels: ["January","February","March","April","May","June","July","August","September","October","November","December"],
                    datasets: [
                    {
                      label : "Average Request",
                      data: Object.values(props.averageRequests),
                      backgroundColor: ['#ffc6ff']
                    },
                    {
                        label : "Not Responded",
                        data: Object.values(props.noResponse),
                        backgroundColor: ['#bee1e6']
                      },
                      {
                        label : "Response time taken more than 3 days",
                        data: Object.values(props.ResponseMoreThan3Days),
                        backgroundColor: ['#e9c46a']
                      }
                    ]
                    
                }}
                options={{
                    maintainAspectRatio: true, 
                    indexAxis: 'x',
                    responsive: true,
                }}
            />
            
        </div>
    );
};

export default VerticalBarChart;
