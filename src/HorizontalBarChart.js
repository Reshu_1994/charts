import * as React from 'react';
import { Bar } from "react-chartjs-2";

const HorizontalBarChart = (props) => {
    const [verbalResponse, setVerbalResponse] = React.useState([]);
    const [writtenReponse, setWrittenReponse] = React.useState([]);
    console.log("HorizontalBarChartProps", props.responseArray);
    
    React.useEffect(() => {
        if(Object.entries(props.responseArray).length !== 0){
            let verbalResp = [];
            let writtenResp = [];
            for(let obj in Object.entries(props.responseArray)){
                verbalResp.push(Object.values(props.responseArray)[obj]["verbal"]);
                writtenResp.push(Object.values(props.responseArray)[obj]["written"]);
            }
            setVerbalResponse(verbalResp);
            setWrittenReponse(writtenResp);
        }
    },[props.responseArray]);
    
    return (
        <>
        <h6>Response based on Country</h6>
        <div className="align-items-center shadow-box" style={{height: "300px",width:"100%"}}>
            
            <Bar 
                data={{
                    labels: Object.keys(props.responseArray),
                    datasets: [
                    {
                      label : "Verbal Response",
                      data: verbalResponse,
                      backgroundColor: ['#ffc6ff']
                    },
                    {
                        label : "Written Response",
                        data: writtenReponse,
                        backgroundColor: ['#bee1e6']
                      }
                    ]
                    
                }}
                options={{
                    maintainAspectRatio: true, 
                    indexAxis: 'y',
                    responsive: true,
                }}
                
            />
            
        </div>
        </>
    );
};

// const config = {
    //     type: 'bar',
    //     data: data,
    //     options: {
    //       indexAxis: 'y',
    //       // Elements options apply to all of the options unless overridden in a dataset
    //       // In this case, we are setting the border of each horizontal bar to be 2px wide
    //       elements: {
    //         bar: {
    //           borderWidth: 2,
    //         }
    //       },
    //       responsive: true,
    //       plugins: {
    //         legend: {
    //           position: 'right',
    //         },
    //         title: {
    //           display: true,
    //           text: 'Chart.js Horizontal Bar Chart'
    //         }
    //       }
    //     },
    //   };

export default HorizontalBarChart;
