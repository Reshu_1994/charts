import Graph from "react-vis-network-graph";
import React, { useState } from "react";
import ReactDOM from "react-dom";

const options = {
  layout: {
    hierarchical: false
  },
  edges: {
    color: "#000000"
  }
};

function randomColor() {
  const red = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  const green = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  const blue = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  return `#${red}${green}${blue}`;
}

const NetworkGraph = () => {
  const createNode = (x, y) => {
    const color = randomColor();
    setState(({ graph: { nodes, edges }, counter, ...rest }) => {
      const id = counter + 1;
      const from = Math.floor(Math.random() * (counter - 1)) + 1;
      return {
        graph: {
          nodes: [
            ...nodes,
            { id, label: `Node ${id}`, color, x, y }
          ],
          edges: [
            ...edges,
            { from, to: id }
          ]
        },
        counter: id,
        ...rest
      }
    });
  }
  const [state, setState] = useState({
    counter: 5,
    graph: {
      nodes: [
        { id : 0 , label: "Product", color: "#0466c8" },
        { id: 1, label: "P1", color: "#0466c8" },
        { id: 2, label: "P2", color: "#0466c8" },
        { id: 3, label: "P3", color: "#0466c8" },
        { id: 4, label: "P4", color: "#0466c8" },
        { id: 5, label: "P5", color: "#0466c8" },
        { id: 6, label: "P6", color: "#0466c8" },
        {id : 21, label: "Product Type: Detail", color : "#ffca3a"},
        {id : 22, label: "UK", color :"#f7b267"},
        {id : 23, label: "US", color :"#f7b267"},
        {id : 24, label: "Canada", color :"#f7b267"},
        {id : 25, label: "Brazil", color :"#f7b267"},
        {id : 26, label: "Austrailia", color :"#f7b267"},
        {id : 27, label: "Infection", color :"#f4845f"},
        {id : 28, label: "Metabolics", color :"#f4845f"},
        {id : 29, label: "Oncology", color :"#f4845f"},
        {id : 30, label: "Respiratory", color :"#f4845f"},
      ],
      edges: [
        { from : 0, to: 1},
        { from: 0, to: 2 },
        { from: 0, to: 3 },
        { from: 0, to: 4 },
        { from: 0, to: 5 },
        { from: 0, to: 6 },
        { from: 1, to: 21 },
        { from: 2, to: 21 },
        { from: 3, to: 21 },
        { from: 6, to: 21 },
        { from: 1, to: 22 },
        { from: 1, to: 26 },
        { from: 2, to: 22 },
        { from: 2, to: 23 },
        { from: 2, to: 25 },
        { from: 2, to: 26 },
        { from: 3, to: 23 },
        { from: 3, to: 24 },
        { from: 3, to: 22 },
        { from: 4, to: 26 },
        { from: 4, to: 22 },
        { from: 5, to: 22 },
        { from: 6, to: 22 },
        { from: 1, to: 29 },
        { from: 1, to: 30 },
        { from: 2, to: 29 },
        { from: 2, to: 27 },
        { from: 3, to: 28 },
        { from: 3, to: 30 },
        { from: 4, to: 28 },
        { from: 4, to: 29 },
        { from: 5, to: 29 },
        { from: 6, to: 27 },
      ]
    },
    events: {
      select: ({ nodes, edges }) => {
        alert("Selected node: " + nodes);
      },
      doubleClick: ({ pointer: { canvas } }) => {
        createNode(canvas.x, canvas.y);
      }
    }
  })
  const { graph, events } = state;
  return (
    <div>
      <h6>{"Network Graph -> Product -> Product Type -> Therapeutic area -> Country"}</h6>
      <Graph graph={graph} options={options} events={events} style={{ height: "400px" }} />
    </div>
  );

}

export default NetworkGraph;