import { height } from '@mui/system';
import * as React from 'react';
import { Doughnut } from "react-chartjs-2";

const LargerDoughnut = (props) => {
    console.log("DoughnutProps", props);
    return (
        <>
        
        <div className="d-flex align-items-center shadow-box" style={{height : "300px",width:"100%"}}>
            <Doughnut 
                data={{
                    labels: [props.prodLabel],
                    datasets: [
                    {
                      label : [props.prodLabel],
                      data: [props.data],
                      backgroundColor: [props.color]
                    }]                    
                }}
                options={{
                    maintainAspectRatio: true, 
                    responsive: true,
                    cutout: "80%"
                }}
            />
            
        </div>
        </>
    );
};

export default LargerDoughnut;
