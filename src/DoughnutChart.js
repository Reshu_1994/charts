import * as React from 'react';
import { Doughnut } from "react-chartjs-2";

const DoughnutChart = (props) => {
    console.log("DoughnutProps", props);
    return (
        <>
        
        <div className="align-items-center shadow-box" style={{height: "65px",width:"50px"}}>
        <label style={{paddingLeft:"17px",fontSize:"14px"}}>{props.prodLabel}</label>
            <Doughnut 
                data={{
                    // labels: [props.prodLabel],
                    datasets: [
                    {
                      label : [props.prodLabel],
                      data: [props.data],
                      backgroundColor: [props.color]
                    }]                    
                }}
                options={{
                    maintainAspectRatio: true, 
                    responsive: true,
                                    
                }}
            />
            
        </div>
        </>
    );
};

export default DoughnutChart;
