import * as React from 'react';
import { Pie } from "react-chartjs-2";

const PieChart = (props) => {

    return (
        <>
        <h6 style={{marginLeft: "50px"}}>Highest Request Rate</h6>
        <div className="d-flex align-items-center shadow-box" style={{height : "300px",width:"76%",marginLeft: "50px"}}>
            <Pie 
                data={{
                    labels: Object.keys(props.highRequestRate),
                    datasets: [
                    {
                      label : "Request Rate",
                      data: Object.values(props.highRequestRate),
                      backgroundColor: ['#ffadad','#ffd6a5','#caffbf','#9bf6ff','#a0c4ff','#cdb4db']
                    }]
                    
                }}
                options={{
                    animation: {
                        animateScale: true
                    },
                    maintainAspectRatio: true, 
                    responsive: true,
                }}
            />
            
        </div>
        </>
    );
};

export default PieChart;
